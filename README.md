# CI test for Arch Packages

The scope of the repo is to test a CI system for Archlinux packages.
The "upstream" package is located at: https://github.com/gun1x/arch-ci-test

Every time this repo gets updated, Gitlab CI triggers a docker container to test the installation and the testing of the "arch-ci-test" software.
In order for this to be fully automated, a script can be configured on a server to answer to the github upstream feed by triggering this packaging repo.
