pacman -Syu --noconfirm
pacman -S sudo binutils fakeroot --noconfirm
echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
useradd archcitest
mkdir /home/archcitest
cd /home/archcitest
curl -o /home/archcitest/PKGBUILD https://gitlab.com/gun1x/arch-ci-test/raw/master/PKGBUILD
chown -R archcitest:archcitest /home/archcitest
usermod -a -G wheel archcitest
sudo -u archcitest makepkg -si --noconfirm
